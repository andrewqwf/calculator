class Calculator:
    '''
    Калькулятор
    '''
    # Первый операнд
    __first_value = None
    # Второй операнд
    __second_value = None

    @property
    def first_value(self):
        return self.__first_value

    @property
    def second_value(self):
        return self.__second_value

    @first_value.setter
    def first_value(self, value):
        if not value.isdigit():
            raise ValueError
        self.__first_value = value

    @second_value.setter
    def second_value(self, value):
        if not value.isdigit():
            raise ValueError
        self.__second_value = value

    def run_command(self, command):
        return eval(f"{self.__first_value}{command}{self.__second_value}")
